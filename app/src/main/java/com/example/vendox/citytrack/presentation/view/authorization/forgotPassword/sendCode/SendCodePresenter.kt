package com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendCode

import android.util.Log
import com.example.vendox.citytrack.domain.useCases.ForgotPasswordUseCase

class SendCodePresenter (private val view: SendCodeView, private val useCase: ForgotPasswordUseCase) {
//    fun sendEmail(email:String){
//        useCase.sendEmail(email)
//                .subscribe({result ->
//                    Log.d("myLogs", result.toString())
//                    view.goToChangePassword()
//                }, {throwable ->
//                    Log.d("myLogs", throwable.message)
//                    view.showError()
//                })
//    }

    fun sendCode(email: String, code:String){
        useCase.sendCode(email, code)
                .subscribe({result ->
                    Log.d("myLogs", result.toString())
                    view.goToChangePassword()
                }, {throwable ->
                    Log.d("myLogs", throwable.message)
                    view.showError()
                })
    }
}