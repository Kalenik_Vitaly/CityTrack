package com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendNewPassword

import android.util.Log
import com.example.vendox.citytrack.domain.useCases.ForgotPasswordUseCase

class SendNewPasswordPresenter(private val view:SendNewPasswordView, private val useCase: ForgotPasswordUseCase) {
    fun sendNewPassword(email:String, password:String){
        useCase.sendNewPassword(email, password)
                .subscribe({result ->
                    Log.d("myLogs", result.toString())
                    Log.d("myLogs", result.toString())
                    view.showSuccess()
                    view.returnToLogin()
                }, {throwable ->
                    Log.d("myLogs", throwable.message)
                    view.showError()
                })
    }
}