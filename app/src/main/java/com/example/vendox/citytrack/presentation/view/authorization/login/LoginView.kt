package com.example.vendox.citytrack.presentation.view.authorization.login

/**
 * Created by Vitaly on 03.04.2018.
 */
interface LoginView {
    fun registrationSuccess()
    fun registrationError()
    fun goToMap()
    fun forgotPassword()
}