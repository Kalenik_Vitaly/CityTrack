package com.example.vendox.citytrack.presentation.view.authorization.registration.firstScreen

/**
 * Created by Vitaly on 02.04.2018.
 */
interface RegistrationView {
    fun registrationSuccess()
    fun registrationError()
    fun goToMap()
    fun continueRegistration()
}