package com.example.vendox.citytrack.presentation.view.authorization.login

import android.util.Log
import com.example.vendox.citytrack.data.repository.UserInfoPreferences
import com.example.vendox.citytrack.domain.dataClasses.request.EmailLogin
import com.example.vendox.citytrack.domain.dataClasses.request.EmailRegistration
import com.example.vendox.citytrack.domain.dataClasses.request.SocNetRegistrationRequest
import com.example.vendox.citytrack.domain.dataClasses.response.UserInfo
import com.example.vendox.citytrack.domain.useCases.LoginUseCase
import com.example.vendox.citytrack.domain.useCases.RegisterUseCase

/**
 * Created by Vitaly on 03.04.2018.
 */
class LoginPresenter(private val view: LoginView,
                     private val registerUseCase: RegisterUseCase,
                     private val loginUseCase: LoginUseCase,
                     private val userInfoPreferences: UserInfoPreferences) {

    fun registerEmail(emailRegistration: EmailRegistration) {
        this.registerUseCase.registerEmail(emailRegistration)
                .subscribe({ result ->
                    Log.d("myLogs", result.toString())
                    view.goToMap()
                }, { throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }

    fun loginEmail(emailLogin: EmailLogin) {
//        view.goToMap()
        this.loginUseCase.loginEmail(emailLogin)
                .subscribe({ result ->
                    Log.d("myLogs", result.toString())
                    if (result.isSuccessful) {
                        view.goToMap()
                        Log.d("myLogs", result.body()!!.size.toString())
                        val user = result.body()!![0]
                        val userInfo = UserInfo(user.id, user.name, user.surname, user.email, "", "", "", "", "", "")
                        userInfoPreferences.setUserInfo(userInfo)
                    }
                }, { throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }

    fun registerVk(socNetRegistrationRequest: SocNetRegistrationRequest) {
        this.registerUseCase.registerVk(socNetRegistrationRequest)
                .subscribe({ result ->
                    Log.d("myLogs", result.toString())
                    view.goToMap()
                }, { throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }

    fun registerFb(socNetRegistrationRequest: SocNetRegistrationRequest) {
        this.registerUseCase.registerFb(socNetRegistrationRequest)
                .subscribe({ result ->
                    Log.d("myLogs", result.toString())
                    view.goToMap()
                }, { throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }

    fun setVkToken(token:String){
        val userInfo = userInfoPreferences.getUserInfo()
        userInfo.vkToken = token
        userInfoPreferences.setUserInfo(userInfo)
    }
}