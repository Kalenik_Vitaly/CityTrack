package com.example.vendox.citytrack.data.repository

import android.content.SharedPreferences
import com.example.vendox.citytrack.domain.dataClasses.response.UserInfo
import com.google.gson.Gson

class UserInfoPreferences(private val sharedPreferences: SharedPreferences) {
        fun setUserInfo(userInfo: UserInfo) {
            val sharedPreferencesEditor = sharedPreferences.edit()
            val json = Gson().toJson(userInfo, UserInfo::class.java)
            sharedPreferencesEditor.putString("userInfo", json)
            sharedPreferencesEditor.apply()
        }

        fun getUserInfo(): UserInfo {
            val json = sharedPreferences.getString("userInfo", "")
            return Gson().fromJson(json, UserInfo::class.java)
        }
}