package com.example.vendox.citytrack.presentation.view.map

import android.location.Location
import com.mapbox.api.directions.v5.models.DirectionsRoute

/**
 * Created by vehdox on 05.04.18.
 */
interface MapView {
    fun enableLocationPlugin()
    fun initializeLocationEngine()
    fun setCameraPosition(location: Location)
    fun drawPolyline(currentRoute: DirectionsRoute)
}