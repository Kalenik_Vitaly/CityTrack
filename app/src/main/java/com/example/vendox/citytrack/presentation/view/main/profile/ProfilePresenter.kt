package com.example.vendox.citytrack.presentation.view.main.profile

import com.example.vendox.citytrack.data.repository.UserInfoPreferences

/**
 * Created by Vitaly on 04.04.2018.
 */
class ProfilePresenter(private val view: ProfileView,
                       private val userInfoPreferences: UserInfoPreferences) {
    fun setUserInfo(){
        view.setUserInfo(userInfoPreferences.getUserInfo())
    }
}