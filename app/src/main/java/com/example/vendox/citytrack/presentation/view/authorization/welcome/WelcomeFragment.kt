package com.example.vendox.citytrack.presentation.view.authorization.welcome

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.vendox.citytrack.presentation.view.authorization.login.LoginFragment
import com.example.vendox.citytrack.presentation.view.authorization.registration.firstScreen.RegistrationFragment
import com.example.vendox.citytrack.R


class WelcomeFragment : Fragment() {
    private lateinit var btnRegister: Button
    private lateinit var btnLogin: Button

//    companion object {
//        fun newInstance(): WelcomeFragment {
//            return WelcomeFragment()
//        }
//    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater?.inflate(R.layout.main_fragment, container, false)

        btnLogin = rootView!!.findViewById(R.id.go_to_login_button)
        btnLogin.setOnClickListener{_ ->
            goToLogin()
        }


        btnRegister = rootView.findViewById(R.id.go_to_registration_button)
        btnRegister.setOnClickListener{_ ->
            goToRegister()
        }
        return rootView
    }

    private fun goToRegister() {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.animator.slide_in_up, R.animator.disappear, R.animator.appear, R.animator.slide_out_bottom)
                .replace(R.id.fragment_container, RegistrationFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun goToLogin() {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.animator.slide_in_up, R.animator.disappear, R.animator.appear, R.animator.slide_out_bottom)
                .replace(R.id.fragment_container, LoginFragment())
                .addToBackStack(null)
                .commit()
    }
}