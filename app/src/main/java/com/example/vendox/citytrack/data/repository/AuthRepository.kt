package com.example.vendox.citytrack.data.repository

import com.example.vendox.citytrack.domain.dataClasses.request.EmailLogin
import com.example.vendox.citytrack.domain.dataClasses.request.EmailRegistration
import com.example.vendox.citytrack.domain.dataClasses.request.ForgotPasswordObject
import com.example.vendox.citytrack.domain.dataClasses.request.SocNetRegistrationRequest
import com.example.vendox.citytrack.domain.dataClasses.response.EmailLoginResponse
import com.example.vendox.citytrack.domain.dataClasses.response.SocNetRegistrationResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response


interface AuthRepository {
    fun registerEmail(registrationObject: EmailRegistration):Observable<ResponseBody>
    fun loginEmail(loginObject: EmailLogin):Observable<Response<List<EmailLoginResponse>>>
    fun registerVk(registrationObject: SocNetRegistrationRequest):Observable<Response<SocNetRegistrationResponse>>
    fun registerVkCheat(socNetRegistrationRequest:SocNetRegistrationRequest):Observable<Response<SocNetRegistrationResponse>>
    fun registerFb(registrationObject: SocNetRegistrationRequest):Observable<Response<SocNetRegistrationResponse>>
    fun forgotPasswordSendEmail(forgotPasswordObject:ForgotPasswordObject): Observable<ResponseBody>
    fun forgotPasswordSendCode(forgotPasswordObject:ForgotPasswordObject): Observable<ResponseBody>
    fun sendNewPassword(emailLogin: EmailLogin): Observable<ResponseBody>
}