package com.example.vendox.citytrack.domain.dataClasses.response

data class JoinedFriends (val name:String,
                          val city:String,
                          val image:String)