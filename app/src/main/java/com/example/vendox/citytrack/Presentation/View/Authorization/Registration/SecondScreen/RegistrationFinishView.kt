package com.example.vendox.citytrack.presentation.view.authorization.registration.secondScreen

/**
 * Created by Vitaly on 01.04.2018.
 */
interface RegistrationFinishView {
    fun registrationSuccess()
    fun registrationError()
    fun goToMap()
}