package com.example.vendox.citytrack.domain.dataClasses.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EmailLogin(@SerializedName("email") @Expose val email: String,
                      @SerializedName("password") @Expose val password: String)