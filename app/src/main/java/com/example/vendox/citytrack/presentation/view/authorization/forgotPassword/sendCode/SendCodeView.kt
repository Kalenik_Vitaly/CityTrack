package com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendCode

interface SendCodeView {
    fun showError()
    fun goToChangePassword()
}