package com.example.vendox.citytrack.domain.dataClasses.response

data class InviteFriends (val name:String,
                          val image:String,
                          val socialNetwork:String)