package com.example.vendox.citytrack.data

import com.example.vendox.citytrack.domain.dataClasses.request.EmailLogin
import com.example.vendox.citytrack.domain.dataClasses.request.EmailRegistration
import com.example.vendox.citytrack.domain.dataClasses.request.ForgotPasswordObject
import com.example.vendox.citytrack.domain.dataClasses.request.SocNetRegistrationRequest
import com.example.vendox.citytrack.domain.dataClasses.response.EmailLoginResponse
import com.example.vendox.citytrack.domain.dataClasses.response.SocNetRegistrationResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import com.google.gson.GsonBuilder




interface AuthApi {

    @POST("/register")
    fun registerEmail(@Body body: EmailRegistration): Observable<ResponseBody>

    @Headers("Accept: application/json")
    @POST("/networks")
    fun registerSocialNetwork(@Body body:SocNetRegistrationRequest): Observable<Response<SocNetRegistrationResponse>>

    @Headers("Accept: application/json")
    @POST("/networks")
    fun registerSocialNetworkCheat(@Body body:SocNetRegistrationRequest): Observable<Response<SocNetRegistrationResponse>>



    @POST("/pass-reset/sendemail")
    fun sendEmail(@Body body: ForgotPasswordObject): Observable<ResponseBody>

    @POST("/pass-reset/codeconfirm")
    fun sendCode(@Body body: ForgotPasswordObject): Observable<ResponseBody>

    @POST("/changepass")
    fun sendNewPassword(@Body body: EmailLogin): Observable<ResponseBody>

    @POST("/login")
    fun loginEmail(@Body body: EmailLogin): Observable<Response<List<EmailLoginResponse>>>


    companion object {
        fun create(): AuthApi {
            val gson = GsonBuilder()
                    .setLenient()
                    .create()

            val retrofit = Retrofit.Builder()
                    .baseUrl("http://citytrack.online/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            return  retrofit.create(AuthApi::class.java)
        }
    }

}