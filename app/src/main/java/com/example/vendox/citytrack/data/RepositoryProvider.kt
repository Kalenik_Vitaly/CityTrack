package com.example.vendox.citytrack.data

import com.example.vendox.citytrack.data.repository.AuthRepository
import com.example.vendox.citytrack.data.repository.AuthRepositoryImpl


class RepositoryProvider {
    companion object {
        fun getAuthRepository(): AuthRepository {
            return AuthRepositoryImpl()
        }

    }
}