package com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendEmail

interface SendEmailView {
    fun goToSendCode()
    fun showError()
}