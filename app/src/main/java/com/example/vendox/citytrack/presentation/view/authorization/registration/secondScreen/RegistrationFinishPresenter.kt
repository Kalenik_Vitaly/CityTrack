package com.example.vendox.citytrack.presentation.view.authorization.registration.secondScreen

import android.util.Log
import com.example.vendox.citytrack.domain.dataClasses.request.EmailRegistration
import com.example.vendox.citytrack.domain.useCases.RegisterUseCase


class RegistrationFinishPresenter(private val view: RegistrationFinishView, private val registerUseCase: RegisterUseCase) {

    fun registerEmail(emailRegistration: EmailRegistration) {
        this.registerUseCase.registerEmail(emailRegistration)
                .subscribe({ result ->
                    Log.d("myLogs", result.toString())
                    
                    view.goToMap()
                }, { throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }
}