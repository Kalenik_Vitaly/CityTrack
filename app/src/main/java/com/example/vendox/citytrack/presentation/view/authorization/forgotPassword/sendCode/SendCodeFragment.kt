package com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendCode

import android.os.Bundle
import android.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.vendox.citytrack.data.RepositoryProvider
import com.example.vendox.citytrack.domain.useCases.ForgotPasswordUseCase
import com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendNewPassword.SendNewPasswordFragment

import com.example.vendox.citytrack.R
import com.rengwuxian.materialedittext.MaterialEditText


class SendCodeFragment : Fragment(), SendCodeView {
    private lateinit var sendCodeButton:Button
    private  lateinit var code:MaterialEditText
    private  lateinit var email:MaterialEditText
    private  lateinit var presenter: SendCodePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_send_code, container, false)

        code = view.findViewById(R.id.forgot_password_code)
        email = view.findViewById(R.id.forgot_password_email)

        val bundle = this.arguments
        if (bundle != null) {
            email.setText(bundle.getString("email"))
        }
        val registerUseCase = ForgotPasswordUseCase(RepositoryProvider.getAuthRepository())
        presenter = SendCodePresenter(this, registerUseCase)

        sendCodeButton = view.findViewById(R.id.btn_send_code)
        sendCodeButton.setOnClickListener { _ ->
            presenter.sendCode(email.text.toString(), code.text.toString())
        }

        return view
    }

    override fun goToChangePassword() {
        val sendCodeFragment = SendNewPasswordFragment()
        val bundle = Bundle()
        bundle.putString("email", email.text.toString())
        sendCodeFragment.arguments = bundle

        fragmentManager.beginTransaction()
                .setCustomAnimations(R.animator.slide_in_from_right, R.animator.disappear, R.animator.appear, R.animator.slide_out_to_right)
                .replace(R.id.fragment_container, sendCodeFragment)
                .addToBackStack(null)
                .commit()
    }

    override fun showError() {
        Toast.makeText(activity, "Ошибка", Toast.LENGTH_SHORT).show()
    }
}
