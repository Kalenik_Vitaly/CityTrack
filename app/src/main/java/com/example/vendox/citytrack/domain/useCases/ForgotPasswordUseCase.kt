package com.example.vendox.citytrack.domain.useCases

import com.example.vendox.citytrack.data.repository.AuthRepository
import com.example.vendox.citytrack.domain.dataClasses.request.EmailLogin
import com.example.vendox.citytrack.domain.dataClasses.request.ForgotPasswordObject
import io.reactivex.Observable
import okhttp3.ResponseBody

class ForgotPasswordUseCase(private val authRepository: AuthRepository) {
    fun sendEmail(email: String): Observable<ResponseBody> {
        return authRepository.forgotPasswordSendEmail(ForgotPasswordObject(email, ""))
    }

    fun sendCode(email: String, code: String): Observable<ResponseBody> {
        return authRepository.forgotPasswordSendCode(ForgotPasswordObject(email, code))
    }

    fun sendNewPassword(email: String, password: String): Observable<ResponseBody> {
        return authRepository.sendNewPassword(EmailLogin(email, password))
    }
}