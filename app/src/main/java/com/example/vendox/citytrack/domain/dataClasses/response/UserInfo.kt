package com.example.vendox.citytrack.domain.dataClasses.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserInfo (@SerializedName("id") @Expose var id:Int,
                     @SerializedName("name") @Expose var name:String,
                     @SerializedName("surname") @Expose var surname:String,
                     @SerializedName("email") @Expose var email:String,
                     @SerializedName("vk_id") @Expose var vkId:String,
                     @SerializedName("facebook_id") @Expose var fbId:String,
                     @SerializedName("password") @Expose var password:String,
                     @SerializedName("phone") @Expose var phone:String,
                     @SerializedName("role") @Expose var role:String,
                     @SerializedName("vkToken") @Expose var vkToken:String)