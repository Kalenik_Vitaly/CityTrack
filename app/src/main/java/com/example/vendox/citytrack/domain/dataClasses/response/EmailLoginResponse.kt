package com.example.vendox.citytrack.domain.dataClasses.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EmailLoginResponse(@SerializedName("id") @Expose val id:Int,
                              @SerializedName("name") @Expose val name:String,
                              @SerializedName("surname") @Expose val surname:String,
                              @SerializedName("email") @Expose val email:String,
                              @SerializedName("token") @Expose val token:String)