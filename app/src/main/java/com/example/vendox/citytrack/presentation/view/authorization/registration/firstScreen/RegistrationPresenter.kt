package com.example.vendox.citytrack.presentation.view.authorization.registration.firstScreen

import android.util.Log
import com.example.vendox.citytrack.domain.dataClasses.request.SocNetRegistrationRequest
import com.example.vendox.citytrack.domain.useCases.RegisterUseCase

/**
 * Created by Vitaly on 02.04.2018.
 */
class RegistrationPresenter(private val view: RegistrationView, private val registerUseCase: RegisterUseCase) {
    fun continueRegistration(){
        view.continueRegistration()
    }

    fun registerVk(socNetRegistrationRequest: SocNetRegistrationRequest) {
        this.registerUseCase.registerVk(socNetRegistrationRequest)
                .subscribe({result ->
                    Log.d("myLogs", result.toString())
                    view.goToMap()
                }, {throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }

    fun registerFb(socNetRegistrationRequest: SocNetRegistrationRequest) {
        this.registerUseCase.registerFb(socNetRegistrationRequest)
                .subscribe({result ->
                    Log.d("myLogs", result.toString())
                    view.goToMap()
                }, {throwable ->
                    Log.d("myLogs", throwable.message)
                    view.registrationError()
                })
    }
}