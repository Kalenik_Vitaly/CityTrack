package com.example.vendox.citytrack.domain.useCases

import com.example.vendox.citytrack.data.repository.AuthRepository
import com.example.vendox.citytrack.domain.dataClasses.request.EmailRegistration
import com.example.vendox.citytrack.domain.dataClasses.request.SocNetRegistrationRequest
import com.example.vendox.citytrack.domain.dataClasses.response.SocNetRegistrationResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response


class RegisterUseCase(private val mRepository: AuthRepository) {
    fun registerEmail(emailRegistration: EmailRegistration) : Observable<ResponseBody>{
        return mRepository.registerEmail(emailRegistration)
    }

    fun registerVk(registrationObject: SocNetRegistrationRequest):Observable<Response<SocNetRegistrationResponse>>{
        return mRepository.registerVk(registrationObject)
    }

    fun registerFb(registrationObject: SocNetRegistrationRequest):Observable<Response<SocNetRegistrationResponse>>{
        return mRepository.registerFb(registrationObject)
    }

}