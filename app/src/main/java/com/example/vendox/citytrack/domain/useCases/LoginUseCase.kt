package com.example.vendox.citytrack.domain.useCases

import com.example.vendox.citytrack.data.repository.AuthRepository
import com.example.vendox.citytrack.domain.dataClasses.request.EmailLogin
import com.example.vendox.citytrack.domain.dataClasses.response.EmailLoginResponse
import retrofit2.Response
import io.reactivex.Observable

class LoginUseCase(private val mRepository: AuthRepository) {
    fun loginEmail(emailLogin: EmailLogin):Observable<Response<List<EmailLoginResponse>>>{
        return mRepository.loginEmail(emailLogin)
    }
}