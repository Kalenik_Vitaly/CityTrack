package com.example.vendox.citytrack.presentation.view.authorization.forgotPassword.sendNewPassword

interface SendNewPasswordView {
    fun showError()
    fun returnToLogin()
    fun showSuccess()
}