package com.example.vendox.citytrack.presentation.view.main.friends

import android.os.Bundle
import android.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.vendox.citytrack.domain.dataClasses.response.InviteFriends
import com.example.vendox.citytrack.domain.dataClasses.response.JoinedFriends
import com.example.vendox.citytrack.presentation.view.main.MapBoxActivity

import com.example.vendox.citytrack.R

class FriendsFragment : Fragment() {
    private lateinit var recyclerViewFriendsToInvite: RecyclerView
    private lateinit var friendsInviteRecyclerViewAdapter: FriendsToInviteRecyclerViewAdapter
    private val dataFriends= ArrayList<InviteFriends>()

    private lateinit var recyclerViewJoinedFriends:RecyclerView
    private lateinit var joinedFriendsRecyclerViewAdapter: JoinedFriendsRecyclerViewAdapter
    private val dataJoinedFriends = ArrayList<JoinedFriends>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_friends, container, false)

        val friend = InviteFriends("Анна Подображных", "https://pp.userapi.com/c824600/v824600803/65eb2/ABeNOlFo_sI.jpg", "Ваш друг с Facebook")
        dataFriends.add(friend)
        recyclerViewFriendsToInvite = view.findViewById(R.id.recycler_view_friends_to_invite)
        recyclerViewFriendsToInvite.layoutManager = LinearLayoutManager(activity)
        recyclerViewFriendsToInvite.isNestedScrollingEnabled = false
        friendsInviteRecyclerViewAdapter = FriendsToInviteRecyclerViewAdapter(dataFriends, activity)
        recyclerViewFriendsToInvite.adapter = friendsInviteRecyclerViewAdapter

        var joinedFriend = JoinedFriends("Виталий Каленик", "Москва", "http://marketline.com/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png")
        dataJoinedFriends.add(joinedFriend)
        joinedFriend = JoinedFriends("Никита Ходнев", "Москва", "https://pp.userapi.com/c626226/v626226951/21278/SkgZmQ8DQtY.jpg")
        dataJoinedFriends.add(joinedFriend)
        joinedFriend = JoinedFriends("Клим Гавриленко", "Москва", "https://sun1-14.userapi.com/c840521/v840521926/2acbe/O4XTrYCX26k.jpg")
        dataJoinedFriends.add(joinedFriend)

        recyclerViewJoinedFriends = view.findViewById(R.id.recycler_view_joined_friends)
        recyclerViewJoinedFriends.layoutManager = LinearLayoutManager(activity)
        recyclerViewJoinedFriends.isNestedScrollingEnabled = false
        joinedFriendsRecyclerViewAdapter = JoinedFriendsRecyclerViewAdapter(dataJoinedFriends, activity)
        recyclerViewJoinedFriends.adapter = joinedFriendsRecyclerViewAdapter


        return view
    }

    override fun onResume() {
        super.onResume()
        (activity as MapBoxActivity).setActionBarTitle("Друзья")
    }

}
