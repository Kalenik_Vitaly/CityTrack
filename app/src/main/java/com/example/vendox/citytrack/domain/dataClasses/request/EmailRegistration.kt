package com.example.vendox.citytrack.domain.dataClasses.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class EmailRegistration(@SerializedName("name") @Expose val name: String,
                             @SerializedName("surname") @Expose val surname: String,
                             @SerializedName("email") @Expose val email: String,
                             @SerializedName("password") @Expose val password: String)