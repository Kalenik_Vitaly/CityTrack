package com.example.vendox.citytrack.presentation.view.main.profile

import com.example.vendox.citytrack.domain.dataClasses.response.UserInfo

/**
 * Created by Vitaly on 04.04.2018.
 */
interface ProfileView {
    fun setUserInfo(userInfo: UserInfo)
}