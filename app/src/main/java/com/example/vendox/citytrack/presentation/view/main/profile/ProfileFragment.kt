package com.example.vendox.citytrack.presentation.view.main.profile

import android.os.Bundle
import android.app.Fragment
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.vendox.citytrack.data.repository.UserInfoPreferences
import com.example.vendox.citytrack.domain.dataClasses.response.UserInfo
import com.example.vendox.citytrack.presentation.view.main.MapBoxActivity

import com.example.vendox.citytrack.R

class ProfileFragment : Fragment(), ProfileView {
    private lateinit var presenter: ProfilePresenter
    private lateinit var recyclerViewCities: RecyclerView
    private lateinit var citiesRecyclerViewAdapter: CitiesRecyclerViewAdapter
    private lateinit var userName:TextView
    private lateinit var userEmail:TextView
    private val dataCities = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        userName = view.findViewById(R.id.profile_user_name)
        userEmail = view.findViewById(R.id.profile_user_email)

        dataCities.add("Москва")
        dataCities.add("Барселона")

        recyclerViewCities = view.findViewById(R.id.profile_rv_cities)
        recyclerViewCities.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        citiesRecyclerViewAdapter = CitiesRecyclerViewAdapter(dataCities, activity)
        recyclerViewCities.adapter = citiesRecyclerViewAdapter


        val sharedPreferences = activity.getSharedPreferences("userInfo", Context.MODE_PRIVATE)
        val userInfoPreferences = UserInfoPreferences(sharedPreferences)
        presenter = ProfilePresenter(this, userInfoPreferences)
        presenter.setUserInfo()

        return view
    }

    override fun onResume() {
        (activity as MapBoxActivity).setActionBarTitle("Профиль")
        super.onResume()
    }

    override fun setUserInfo(userInfo: UserInfo) {
        userName.text = userInfo.name + " " + userInfo.surname
        userEmail.text = userInfo.email
    }


}
